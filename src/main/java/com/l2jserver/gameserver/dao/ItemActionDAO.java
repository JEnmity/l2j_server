package com.l2jserver.gameserver.dao;

public interface ItemActionDAO {

    void queue(int sender, int receiver, String type, int objectId, int itemId, long count);
}
