package com.l2jserver.gameserver.dao.impl.mysql;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.dao.ItemActionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Item trade action DAO MySQL Implementation
 * @author Enmity
 */
public class ItemActionDAOMySQLImpl implements ItemActionDAO, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ItemActionDAOMySQLImpl.class);
    private static final List<LogEntry> logQueue = Collections.synchronizedList(new LinkedList<LogEntry>());
    private static final String INSERT = "INSERT INTO item_actions (timestamp, oldOwner, newOwner, type, objectid, itemid, count) values (?,?,?,?,?,?,?)";

    public ItemActionDAOMySQLImpl()
    {
        LOG.info("Item Action logger enabled.");
        ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(this, 60000, 60000);
    }
    /**
     *  Saves a log into db
     */
    @Override
    public synchronized void run() {
        if (logQueue.size() > 0) {
            try (Connection con = ConnectionFactory.getInstance().getConnection();
                 PreparedStatement ps = con.prepareStatement(INSERT)) {
                ListIterator<LogEntry> iterator = logQueue.listIterator();
                LogEntry logEntry;
                while (iterator.hasNext()) {
                    logEntry = iterator.next();
                    ps.setTimestamp(1, logEntry.timestamp);
                    ps.setInt(2, logEntry.oldOwner);
                    ps.setInt(3, logEntry.newOwner);
                    ps.setString(4, logEntry.type);
                    ps.setInt(5, logEntry.objectId);
                    ps.setInt(6, logEntry.itemId);
                    ps.setLong(7, logEntry.count);
                    iterator.remove();
                    ps.addBatch();
                }
                ps.executeBatch();
            } catch (SQLException e) {
                LOG.warn("Could not save item action data between players: {}", e);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void queue(int oldOwner, int newOwner, String type, int objectId, int itemId, long count)
    {
        LogEntry logEntry = new LogEntry();
        logEntry.timestamp = new Timestamp(System.currentTimeMillis());
        logEntry.oldOwner = oldOwner;
        logEntry.newOwner = newOwner;
        logEntry.type = type;
        logEntry.objectId = objectId;
        logEntry.itemId = itemId;
        logEntry.count = count;
        logQueue.add(logEntry);
    }

    private class LogEntry {
        Timestamp timestamp;
        int oldOwner;
        int newOwner;
        String type;
        int objectId;
        int itemId;
        long count;
    }
}
